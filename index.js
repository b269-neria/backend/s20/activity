// [SECTION] Activity part 1

let userInput = Number(prompt("Please input a number: "));

console.log("The number you have provided is "+userInput+".");
for(userInput;userInput > 0;userInput--){

	if(userInput < 50){
		console.log("Please input a value higher than 50");
		break;
	}

	if(userInput % 10 === 0 && userInput !==50){
		console.log("This number is divisible by 10. Skipping the number.");
	} else if (userInput % 5 === 0 && userInput !==50){
		console.log(userInput);
		continue;
	} else if(userInput === 50){
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

}

// [SECTION] Activity part 2

let superString = "supercalifragilisticexpialidocious";
console.log(superString);
let superStringConsonants = "";


for (let i = 0; i < superString.length; i++){

	if (superString[i].toLowerCase() === "a" ||
		superString[i].toLowerCase() === "e" ||
		superString[i].toLowerCase() === "i" ||
		superString[i].toLowerCase() === "o" ||
		superString[i].toLowerCase() === "u") {
		continue;
	} else{
		superStringConsonants += superString[i];
	}


		
		
}

console.log(superStringConsonants);